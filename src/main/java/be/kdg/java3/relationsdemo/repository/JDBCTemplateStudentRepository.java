package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.*;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbctemplate")
public class JDBCTemplateStudentRepository implements StudentRepository {
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert studentInserter;

	public JDBCTemplateStudentRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.studentInserter = new SimpleJdbcInsert(jdbcTemplate).withTableName("STUDENTS").usingGeneratedKeyColumns(
			"ID");
	}

	//Helper method: maps the columns of the DB to the attributes of the Student
	public Student mapStudentWithAddressRow(ResultSet rs, int rowid) throws SQLException {
		return new Student(rs.getInt("ID"),
			rs.getString("NAME"),
			rs.getDouble("LENGTH"),
			rs.getDate("BIRTHDAY").toLocalDate(),
			(rs.getString("CITY") != null) ? new Address(rs.getString("STREET"),
				rs.getInt("POSTAL_CODE"),
				rs.getString("CITY")) : null);
	}

	//instance method, this is a singleton
	public Student mapStudentWithAddressAndSchoolRow(ResultSet rs, int rowid) throws SQLException {
		return new Student(rs.getInt("STUDENTS.ID"),
			rs.getString("STUDENTS.NAME"),
			rs.getDouble("LENGTH"),
			rs.getDate("BIRTHDAY").toLocalDate(),
			(rs.getString("CITY") != null) ? new Address(rs.getString("STREET"),
				rs.getInt("POSTAL_CODE"),
				rs.getString("CITY")) : null,
			new School(rs.getInt("SCHOOLS.ID"),
				rs.getString("SCHOOLS.NAME"))
		);
	}

	//Helper method: maps the columns of the DB to the attributes of the Student
	public static Student mapStudentRow(ResultSet rs, int rowid) throws SQLException {
		return new Student(rs.getInt("ID"),
			rs.getString("NAME"),
			rs.getDouble("LENGTH"),
			rs.getDate("BIRTHDAY").toLocalDate());
	}

	@Override
	public List<Student> findAll() {
		return jdbcTemplate.query("""
				 SELECT * FROM STUDENTS 
				 JOIN SCHOOLS 
				 ON STUDENTS.SCHOOL_ID = SCHOOLS.ID
				""",
			this::mapStudentWithAddressAndSchoolRow);
	}

	@Override
	public Student findById(int id) {
		Student student = jdbcTemplate.queryForObject(
			"""
				 SELECT * FROM STUDENTS 
				 JOIN SCHOOLS 
				 ON STUDENTS.SCHOOL_ID = SCHOOLS.ID
				  WHERE STUDENTS.ID = ?
				""",
			this::mapStudentWithAddressAndSchoolRow,
			id);
		return student;
	}

	@Override
	public Student createStudent(Student student) {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("NAME", student.getName());
		parameters.put("LENGTH", student.getLength());
		parameters.put("BIRTHDAY", Date.valueOf(student.getBirthday()));
		parameters.put("SCHOOL_ID", student.getSchool().getId());
		Address address = student.getAddress();
		if (address != null) {
			parameters.put("STREET", address.getStreet());
			parameters.put("POSTAL_CODE", address.getPostalCode());
			parameters.put("CITY", address.getCity());
		}
		student.setId(studentInserter.executeAndReturnKey(parameters).intValue());
		return student;
	}

	@Override
	public void updateStudent(Student student) {
		Address address = student.getAddress();
		if (address != null) {
			jdbcTemplate.update(
				"UPDATE STUDENTS SET NAME=?, LENGTH=?,BIRTHDAY=?,SCHOOL_ID=?,STREET=?,POSTAL_CODE=?,CITY=? WHERE ID=?",
				student.getName(),
				student.getLength(),
				Date.valueOf(student.getBirthday()),
				student.getSchool().getId(),
				address.getStreet(),
				address.getPostalCode(),
				address.getCity(),
				student.getId());
		} else {
			jdbcTemplate.update("UPDATE STUDENTS SET NAME=?, LENGTH=?,BIRTHDAY=?,SCHOOL_ID=? WHERE ID=?",
				student.getName(),
				student.getLength(),
				Date.valueOf(student.getBirthday()),
				student.getSchool().getId(),
				student.getId());
		}
	}

	@Override
	public void deleteStudent(int id) {
		jdbcTemplate.update("DELETE FROM STUDENTS WHERE ID=?", id);
	}

	@Override
	public List<Student> findBySchool(int schoolid) {
		return jdbcTemplate.query("""
				 SELECT * FROM STUDENTS
			JOIN SCHOOLS
			ON STUDENTS.SCHOOL_ID = SCHOOLS.ID
			WHERE SCHOOLS.ID = ? 
			""",
			this::mapStudentWithAddressAndSchoolRow,
			schoolid);
	}
}
