package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.School;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
@Profile("jdbctemplate")
public class JDBCTemplateSchoolRepository implements SchoolRepository {
    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert inserter;

    public JDBCTemplateSchoolRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.inserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("SCHOOLS")
                .usingGeneratedKeyColumns("ID");
    }

    //Helper method: maps the columns of the DB to the attributes of the Student
    public static School mapSchoolRow(ResultSet rs, int rowid) throws SQLException {
        return new School(rs.getInt("ID"),
                rs.getString("NAME"));
    }

    @Override
    public List<School> findAll() {
        return jdbcTemplate.query("SELECT * FROM SCHOOLS", JDBCTemplateSchoolRepository::mapSchoolRow);
    }

    @Override
    public School findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM SCHOOLS WHERE ID = ?", JDBCTemplateSchoolRepository::mapSchoolRow, id);
    }

    @Override
    public School create(School school) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", school.getName());
        school.setId(inserter.executeAndReturnKey(parameters).intValue());
        return school;
    }

    @Override
    public void update(School school) {
        jdbcTemplate.update("UPDATE SCHOOLS SET NAME=? WHERE ID=?",
                school.getName(), school.getId());
    }

    @Override
    @Transactional
    public void delete(int id) {
        //should we delete all the students of that school? (school_id cannot be null!)
        //we need to delete the addresses also!
        jdbcTemplate.update("DELETE FROM STUDENTS WHERE SCHOOL_ID=?", id);
        jdbcTemplate.update("DELETE FROM SCHOOLS WHERE ID=?", id);
        //to demonstrate the @Transactional, replace above line by this:
        //jdbcTemplate.update("DELETE FROM SCHOOLS WHERE ID=? AND DESCRIPTION = 'ok'", id);
    }
}
