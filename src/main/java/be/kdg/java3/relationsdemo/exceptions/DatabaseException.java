package be.kdg.java3.relationsdemo.exceptions;

//I'm creating my own Exception class for all DB Exceptions
//It is an unchecked exception (extends RuntimeException)
public class DatabaseException extends RuntimeException{
    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
