package be.kdg.java3.relationsdemo.domain;

import java.time.LocalDate;

public class Student {
	private int id;
	private String name;
	private double length;
	private LocalDate birthday;
	private Address address;
	private School school;


	public School getSchool() {
		return school;
	}

	/**
	 * unidirectional *-1 relation
	 * @param school
	 */
	public void setSchool(School school) {
		this.school = school;
	}


	public Student(int id, String name, double lenght, LocalDate birthday, Address address,School school) {
		this(id, name, lenght, birthday,address);
		setSchool(school);
	}

	public Student(int id, String name, double lenght, LocalDate birthday, Address address) {
		this(id, name, lenght, birthday);
		setAddress(address);
	}

	public Student(String name, double lenght, LocalDate birthday, Address address,School school) {
		this( name, lenght, birthday,address);
		setSchool(school);
	}
	public Student(String name, double lenght, LocalDate birthday, Address address) {
		this( name, lenght, birthday);
		setAddress(address);
	}
	public Student(String name, double lenght, LocalDate birthday, School school) {
		this( name, lenght, birthday);
		setSchool(school);
	}
	public Student(int id, String name, double length, LocalDate birthday) {
		this.id = id;
		this.name = name;
		this.length = length;
		this.birthday = birthday;
	}

	public Student(String name, double length, LocalDate birthday) {
		this.name = name;
		this.length = length;
		this.birthday = birthday;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public Address getAddress() {
		return address;
	}

	/**
	 * Bidirectional relationship with Student as master.
	 * address. We also set the student on address
	 * @param address
	 */
	public void setAddress(Address address) {
		this.address = address;
		if (address != null) {
			address.setStudent(this);
		}
	}

	@Override
	public String toString() {
		return "Student{" +
			"id=" + id +
			", name='" + name + '\'' +
			", lenght=" + length +
			", birthday=" + birthday +
			 (address != null ? " "+address : "") +
			" " + school +
			'}';
	}
}
