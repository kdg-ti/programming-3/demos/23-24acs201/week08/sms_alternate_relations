package be.kdg.java3.relationsdemo.presentation;

public record MenuItem (String text, Runnable action){
}
