package be.kdg.java3.relationsdemo.presentation;

import be.kdg.java3.relationsdemo.domain.*;
import be.kdg.java3.relationsdemo.repository.SchoolRepository;
import be.kdg.java3.relationsdemo.repository.StudentRepository;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.*;
import org.springframework.lang.NonNullApi;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

@Component
public class StudentMenu implements CommandLineRunner, ApplicationContextAware {
	private final Scanner scanner = new Scanner(System.in);
	private final StudentRepository studentRepository;
	private final SchoolRepository schoolRepository;
	private final List<MenuItem> menu = List.of(
		new MenuItem("list all students", this::listAllStudents),
		new MenuItem("add a student", this::addStudent),
		new MenuItem("update a student", this::updateStudent),
		new MenuItem("delete a student", this::deleteStudent),
		new MenuItem("change address of student", this::changeAddressOfStudent),
		new MenuItem("list all schools", this::listAllSchools),
		new MenuItem("add a school", this::addSchool),
		new MenuItem("delete a school", this::deleteSchool),
		new MenuItem("change school of student", this::changeSchoolOfStudent),
		new MenuItem("list all students of school", this::showAllStudentsOfSchool),
		new MenuItem("add course", this::addCourse),
		new MenuItem("delete course", this::deleteCourse),
		new MenuItem("add student to course", this::addStudentToCourse),
		new MenuItem("list students of course", this::listStudentsOfCourse),
		new MenuItem("list courses of student", this::listCoursesOfStudent)
	);

	private void listAllSchools() {
		for(School school: schoolRepository.findAll()) {
			System.out.println(school);
		}
	}

	private ApplicationContext ctx;


	public StudentMenu(StudentRepository studentRepository, SchoolRepository schoolRepository) {
		this.studentRepository = studentRepository;
		this.schoolRepository = schoolRepository;
	}

	@Override
	public void run(String... args)  {
		boolean active = true;
		while (active) {
			System.out.print("""
				Welcome to the Student Management System
				========================================
				0) Exit
				""");
			for (int i = 0; i < menu.size(); i++) {
				System.out.println((i + 1) + ") " + menu.get(i).text());
			}
			System.out.print("Your choice: ");
			int choice = Integer.parseInt(scanner.nextLine());
			if (choice < 0 || choice > menu.size()) {
				System.out.println("Invalid choice");
			} else if (choice == 0) {
				System.out.println("Exiting...");
				active=false;
				((ConfigurableApplicationContext) ctx).close();
			} else {
				menu.get(choice - 1).action().run();
			}
		}
	}

	private void addCourse() {
		//TODO
	}

	private void addSchool() {
		System.out.print("Name of school: ");
		String schoolName = scanner.nextLine();
		System.out.println("Created " + schoolRepository.create(new School(schoolName)));
	}

	private void deleteCourse() {
		//TODO
	}

	private void listCoursesOfStudent() {
		//TODO
	}

	private void listStudentsOfCourse() {
		//TODO
	}

	private void addStudentToCourse() {
		//TODO
	}

	private void deleteSchool() {
		try {
			schoolRepository.delete(askSchoolId());
		} catch (RuntimeException rte) {
			System.out.println("Unable to delete..." + rte.getMessage());
		}
	}

	private int askSchoolId() {
		listAllSchools();
		System.out.print("Id of school: ");
		return Integer.parseInt(scanner.nextLine());
	}

	private void showAllStudentsOfSchool() {
			studentRepository.findBySchool(askSchoolId()).forEach(System.out::println);
	}

	private void changeSchoolOfStudent() {
		Student student = askStudent();
		if (student != null) {
			changeSchoolOfStudent(student);
			studentRepository.updateStudent(student);
		}
	}

	private Student askStudent() {
		return studentRepository.findById(askStudentId());
	}

	private int askStudentId() {
		listAllStudents();
		System.out.print("Student id: ");
		return Integer.parseInt(scanner.nextLine());
	}

	private void changeAddressOfStudent() {
		Student student = askStudent();
		if (student != null) {
			Address address = getAddress();
			if (address != null) {
				student.setAddress(address);
				studentRepository.updateStudent(student);
			}
		}
	}

	private void listAllStudents() {
		try {
			studentRepository.findAll().forEach(System.out::println);
		} catch (RuntimeException dbe) {
			System.out.println("Unable to find all students:");
			System.out.println(dbe.getMessage());
			dbe.printStackTrace();
		}
	}

	private void changeSchoolOfStudent(Student student) {
		School school = askSchool();
		if (school != null) {
			student.setSchool(school);
			System.out.println("Updated student: " + student);
		} else {
			System.out.println("School not found, student unchanged.");
		}
	}

	private School askSchool() {
		return schoolRepository.findById(askSchoolId());
	}

	private void addStudent() {
		Student student = askStudentAttributes(false);
		try {
			Student createdStudent
				= studentRepository.createStudent(student);
			System.out.println("Student added to database:" + createdStudent);
		} catch (RuntimeException dbe) {
			System.out.println("Problem:" + dbe.getMessage());
		}
	}

	private Student askStudentAttributes(boolean askId) {
		int id = 0;
		if (askId) {
			System.out.print("Id:");
			id = Integer.parseInt(scanner.nextLine());
		}
		System.out.print("Name: ");
		String name = scanner.nextLine();
		System.out.print("Length: ");
		double length = Double.parseDouble(scanner.nextLine());
		System.out.print("Birthday (dd-mm-yyyy): ");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
		Address address = getAddress();
		return new Student(id, name, length, birthday, address);
	}

	private Address getAddress() {
		System.out.print("Street: ");
		String street = scanner.nextLine();
		Address address = null;
		if (!street.isBlank()) {
			System.out.print("Postal code: ");
			int postalCode = scanner.nextInt();
			scanner.nextLine();
			System.out.print("City: ");
			String city = scanner.nextLine();
			address = new Address(street, postalCode, city);
		}
		return address;
	}

	private void updateStudent() {
		listAllStudents();
		Student student = askStudentAttributes(true);
		try {
			studentRepository.updateStudent(student);
		} catch (RuntimeException dbe) {
			System.out.println("Problem:" + dbe.getMessage());
		}
	}

	private void deleteStudent() {
		int id = askStudentId();
		try {
			studentRepository.deleteStudent(id);
		} catch (RuntimeException dbe) {
			System.out.println("Problem:" + dbe.getMessage());
			dbe.printStackTrace();
		}
	}

	@Override
	public void setApplicationContext( ApplicationContext applicationContext) throws BeansException {
		ctx = applicationContext;
	}
}
