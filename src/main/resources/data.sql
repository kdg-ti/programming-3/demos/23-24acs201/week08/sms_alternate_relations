INSERT INTO SCHOOLS (ID, NAME) VALUES (1,'KDG'),
                                  (2,'AP'),
                                  (3,'Thomas More');
ALTER TABLE SCHOOLS ALTER COLUMN ID RESTART WITH (SELECT MAX(ID) FROM SCHOOLS) + 1;

INSERT INTO STUDENTS (ID, NAME, LENGTH, BIRTHDAY, STREET, POSTAL_CODE, CITY,SCHOOL_ID)
VALUES (1, 'Jan', 1.89, '1987-04-25', 'wijngaardstraat 12', 2140, 'borgerhout',1),
       (2, 'Jef', 1.81, '1977-04-25', NULL, NULL, NULL,1),
       (3, 'Mie', 1.67, '1967-04-25', 'coquilhatstraat 2', 2000, 'antwerp',2),
       (4, 'Fien', 1.34, '1937-04-25', NULL, NULL, NULL,3),
       (5, 'Erica', 1.56, '1987-05-25', NULL, NULL, NULL,1),
       (6, 'Bart', 2, '1987-04-26', NULL, NULL, NULL,2),
       (7, 'Josefien', 1.89, '1983-04-25', 'cogelslei 7', 2140, 'borgerhout',3),
       (8, 'Elke', 1.8, '1981-04-15', NULL, NULL, NULL,1),
       (9, 'Erna', 1.99, '1982-01-25', 'kleidreef 6', 3000, 'mechelen',3);
ALTER TABLE STUDENTS ALTER COLUMN ID RESTART WITH (SELECT MAX(ID) FROM STUDENTS) + 1;